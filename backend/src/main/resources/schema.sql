DROP TABLE IF EXISTS client_db;
DROP TABLE IF EXISTS bw_list_db;
DROP TABLE IF EXISTS messages_db;
DROP TABLE IF EXISTS folders_db;

create table client_db
(
    email varchar(30) PRIMARY KEY,
    name varchar(20),
    female varchar(30),
    password varchar(30),
    appPassword varchar(30),
    imgUrl bytea,
    city varchar(30),
    nickname varchar(30)
);

create table folders_db(
   clientId varchar(30) references client_db(email) on delete cascade ,
   folderName varchar(12),
   primary key (clientId,folderName)
);

create table roles_db(
   clientId varchar(30) references client_db(email) on delete cascade,
   role varchar(12),
   primary key (clientId,role)
);

create table permissions_db(
    id serial,
    permission varchar(20),
    primary key (id),
    clientId varchar(30) references client_db(email) on delete cascade,
    role varchar(12),
    foreign key (clientId,role) references roles_db(clientId, role) on delete cascade
);

create table messages_db(
    id serial primary key,
    emailFrom varchar(50) not null,
    emailTo varchar(50),
    subject varchar(100),
    sentData timestamp with time zone,
    receivedDate timestamp with time zone,
    clientId varchar(30),
    folderName varchar(12),
    hasAttachment boolean,
    isNew boolean,
    isSeen boolean,
    uid int,
    foreign key (clientId,folderName) references folders_db(clientId,folderName) on delete cascade
);


create table bw_list_db
(
    id serial primary key,
    clientId varchar(30),
    list_member varchar(30),
    foreign key (clientId) references client_db(email) on delete cascade
);
