package com.sendmail.exceptions;

public class BadTokenConnectionException extends Exception{
    public BadTokenConnectionException(String message) {
        super(message);
    }
}
