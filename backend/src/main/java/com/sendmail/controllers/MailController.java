package com.sendmail.controllers;

import com.sendmail.entities.Client;
import com.sendmail.entities.Envelope;
import com.sendmail.entities.Post;
import com.sendmail.entities.ServerMailPort;
import com.sendmail.mail.MailAdminImpl;
import com.sendmail.service.ClientServiceImpl;
import com.sendmail.utills.CommonUtils;
import com.sendmail.utills.MailUtils;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.IMAPProtocol;
import jakarta.mail.*;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("mail")
public class MailController {

    @Autowired
    private MailAdminImpl mailAdmin;

    @Autowired
    private ClientServiceImpl clientServiceImpl;

    @Autowired
    @Qualifier("sessionRegistry")
    private SessionRegistry sessionRegistry;

    @GetMapping("/{folder}") // ?
    public ResponseEntity<String> getFolder(@PathVariable String folder){
        return ResponseEntity.ok().body(""); // ? todo проверка на black list
    }

    @PostMapping(value = "/send", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<String> sendMail(@ModelAttribute Post post,
                                           Authentication authentication
                                           ){
        /// decode jwt to get logpas
        try {
            mailAdmin.sendMail((String) authentication.getPrincipal(), (String) authentication.getCredentials(), post);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body("Sent");
        }
        catch(Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/selected/{email}")
    public ResponseEntity<String> getAllEmailsFromClient(@PathVariable String email){
        return ResponseEntity.ok().body("");
    }
    @GetMapping("/messages/count")
    public int[] getMessageCount(Authentication authentication){
        return clientServiceImpl.getMessagesCount((String)authentication.getPrincipal());
    }

    @GetMapping(value = "/messages/{folder}",params = {"sort","page","size"})
    public List<Envelope> getMessages(@RequestParam("sort") String type,
                                      @PathVariable String folder,
                                      @RequestParam("page") int page,
                                      @RequestParam("size") int size,
                                      Authentication authentication){
        String email = (String)authentication.getPrincipal();
        int offset = page*size;
        int limit = (page-1)*size;
       /* switch (type) {
         *//*   case "attachment" -> {
                clientServiceImpl.showHasAttachment(email,folder,true,offset,limit);
            }
            case "oldest" -> {
                clientServiceImpl.sortByDateAsc(email,folder,offset,limit);
            }
            case ("read") -> {
                clientServiceImpl.showByReadStatus(email,folder,true,offset,limit);
            }*//*
        }*/

        return null;
    }


    @GetMapping(value = "/messages/{folder}",params = {"page","size"})
    public List<Envelope> getMessages(@RequestParam("page") int page,
                                                      @RequestParam("size") int size,
                                                      @PathVariable String folder,
                                                      Authentication authentication){
        String email = (String)authentication.getPrincipal();

        return clientServiceImpl.getAllMessages(email,folder.toUpperCase(),page*size,(page-1)*size);
    }

    @GetMapping(value = "/messages/{folder}")
    public List<Envelope> getMessagesFromAllTopics(
                                      @PathVariable String folder,
                                      Authentication authentication){
        String email = (String)authentication.getPrincipal();

        return clientServiceImpl.getAllMessages(email,folder.toUpperCase(),10000000,0);
    }


    @GetMapping("/messages/{folder}/{id}") // this is uid
    public ResponseEntity<Envelope> getMessages(@PathVariable String folder, @PathVariable long id,
                                                Authentication authentication){
        HashMap<String,String> map = new HashMap<>();
        map.put("email",(String) authentication.getPrincipal());
        map.put("password",(String) authentication.getCredentials());

        return ResponseEntity.ok().body(mailAdmin.getMessageByUID(map,id,folder));
    }

    @PostMapping("/blacklist/block")
    public void putEmailToBlackList(@RequestBody String email,
                                    Authentication authentication){
        clientServiceImpl.insertClientIntoBlackList((String) authentication.getPrincipal(),email);
    }

    @PostMapping("/blacklist/delete")
    public List<String> deleteClientFromBlacklist(@RequestBody String email,
                                    Authentication authentication){
        return clientServiceImpl.deletePersonFromBlacklist((String) authentication.getPrincipal(),email);
    }

    @GetMapping("/blacklist/all")
    public List<String> getBlackList(Authentication authentication){
        return clientServiceImpl.getBlackList((String) authentication.getPrincipal());
    }

  /*  @Scheduled(fixedRate = 60000)
    public void fetchMessagesAndPullThemToCache()
    {
            List<Client> list = CommonUtils.getClientsFromPrincipals(sessionRegistry.getAllPrincipals(), clientServiceImpl);
            for (Client client : list) {
                try {
                    MailUtils.fillMessages(client, mailAdmin);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        System.out.println("Scheduled");
    }*/
}
