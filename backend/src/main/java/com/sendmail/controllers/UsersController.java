package com.sendmail.controllers;

import com.sendmail.entities.Client;
import com.sendmail.exceptions.ClientNotFoundException;
import com.sendmail.mail.MailAdminImpl;
import com.sendmail.service.ClientServiceImpl;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("mail/users")
public class UsersController {

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private MailAdminImpl admin;

    @Autowired
    private ClientServiceImpl clientServiceImpl;

    @GetMapping(value = "/user/{id}")
    public ResponseEntity<String> getUserInfo(@PathVariable long id) throws ClientNotFoundException {
        return ResponseEntity.ok().body(clientServiceImpl.getUserInfo(id));
    }

    @GetMapping("/user/{email}")
    public ResponseEntity<String> getUserInfo(@PathVariable String email){
        return ResponseEntity.ok().body(clientServiceImpl.getUserInfo(email));
    }

    @GetMapping("/me")
    public Client getMyInfo(Authentication authentication){
        Client client = (Client)clientServiceImpl.loadUserByUsername(authentication.getPrincipal().toString());
        client.setPassword(null);

        return client;
    }
    @GetMapping("/me/all")
    public Client getAllMyInfo(Authentication authentication,
                               HttpServletRequest request){
        Client client = (Client)clientServiceImpl.loadUserByUsername(authentication.getPrincipal().toString());

        client.setLastInetAddress(request.getRemoteHost()+request.getRemotePort());
        HashMap<String,String> info = new HashMap<>();
        info.put("email",client.getEmail());
        info.put("password",client.getPassword());
        client.setImgUrl((client.getImgUrl()));

        client.setMesCount(admin.getCountOfAllTopicMessages(info));

        client.setPassword(null);

        return client;
    }


    @PostMapping("/me/delete")
    public void deleteUser(Authentication authentication){
        clientServiceImpl.deleteUser((String)authentication.getPrincipal());
    }

    @PostMapping("/me")
    public void updateMe(@ModelAttribute Client client,
                                           Authentication authentication)
    {
        clientServiceImpl.updateUser((String)authentication.getPrincipal(),client);
    }

    @PostMapping("/me/reset-img")
    public void resetAvatar(Authentication auth,@RequestBody String image){
        clientServiceImpl.updateUserImg((String)auth.getPrincipal(),image.getBytes());
    }
}
