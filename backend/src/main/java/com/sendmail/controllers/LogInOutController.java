package com.sendmail.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sendmail.config.JwtClass;
import com.sendmail.utills.Property;
import com.sendmail.entities.Client;
import com.sendmail.mail.MailAdminImpl;
import com.sendmail.service.ClientServiceImpl;
import com.sendmail.utills.CommonUtils;
import com.sendmail.utills.MailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class LogInOutController {
    @Autowired
    private Property property;

    @Autowired
    private MailAdminImpl mailAdmin;

    @Autowired
    private ClientServiceImpl clientServiceImpl;

    @Autowired
    private SessionRegistry sessionRegistry;

    @GetMapping("/mail/logout")
    public void logout(Authentication authentication, HttpServletRequest request){
        String email =(String)authentication.getPrincipal();

        for(SessionInformation info:sessionRegistry.getAllSessions(email,true)){
            info.expireNow();
            CommonUtils.killExpiredSessionForSure(info.getSessionId());
        }

        SecurityContext context = SecurityContextHolder.getContext();
        context.setAuthentication(null);

        clientServiceImpl.deleteAllMessages(email);
    }

    @GetMapping("/refresh")
    public void refreshJwt(HttpServletRequest request,
                                             HttpServletResponse response) throws IOException {
        String header = request.getHeader(AUTHORIZATION);
        if(header!= null && header.startsWith("Bearer ")){
            try {
                String refresh_token = request.getHeader("Authorization")
                        .substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256(property.getToken());

                JWTVerifier verifier = JWT.require(algorithm).build();

                DecodedJWT decodedJWT = verifier.verify(refresh_token);

                String email = decodedJWT.getSubject();
                String pas =decodedJWT.getClaim("pas").asString();

                HashMap<String,String> tokens = JwtClass.getJwtAccessRefreshTokens(email,pas,property.getToken());
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(),tokens);
                System.out.println("refreshed");
            }
            catch(Exception e){
                response.setStatus(FORBIDDEN.value());
                response.setContentType(APPLICATION_JSON_VALUE);
                HashMap<String,String> error = new HashMap<>();
                error.put("error_response",e.getMessage());
                new ObjectMapper().writeValue(response.getOutputStream(),error);
            }
        }
        else{
            response.setStatus(FORBIDDEN.value());
        }
    }
    @PostMapping(value="/registration",consumes={MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public void registerNewClient(HttpServletRequest request,
                                  HttpServletResponse response) throws IOException {

        HashMap<String,String> body = new HashMap<>();
        body.put("email",request.getParameter("email"));
        body.put("female",request.getParameter("female"));
        body.put("name",request.getParameter("name"));
        body.put("password",request.getParameter("password"));
        body.put("appPassword",request.getParameter("appPassword"));
        Client client = (Client) clientServiceImpl
                .loadUserByUsername(body.get("email"));

        if(client!= null){
            response.setStatus(HttpStatus.FORBIDDEN.value()); // exists or unknown info
        }
        else {
            try {
              //  mailAdmin.login(body); //todo login

                clientServiceImpl.registerClient(body);
                MailUtils.fillMessages(client,mailAdmin);

                HashMap<String, String> tokens = JwtClass
                        .getJwtAccessRefreshTokens(body.get("email"),
                                body.get("password"),property.getToken());

                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            }catch(Exception e){
                response.setStatus(HttpStatus.FORBIDDEN.value()); // exists or unknown info
                System.out.println(e.getMessage());
            }
        }
    }
}
