package com.sendmail.entities;

import lombok.Data;

@Data
public class BlackList {

    private int client;

    private String blackListMember;
}
