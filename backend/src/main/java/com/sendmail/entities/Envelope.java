package com.sendmail.entities;

import com.fasterxml.jackson.annotation.JsonView;
import jakarta.mail.Flags;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Data
@NoArgsConstructor
public class Envelope implements Serializable {
    private String to;

    private String from;

    private String subject;

    private Date receivedDate;

    private Date sentDate;

    private boolean hasAttachment = false;

    private boolean newMessage = false;

    private boolean seen = false;

    private long uid;

    private String bodyText;
    public List<byte[]> byteList = new ArrayList<>();

    public Envelope(String to,
                    String from,
                    String subject,
                    Date receivedDate,
                    Date sentDate,
                    boolean hasAttachment,
                    boolean newMessage,
                    boolean seen,
                    long uid) {
        this.to = to;
        this.from = from;
        this.subject = subject;
        this.receivedDate = receivedDate;
        this.sentDate = sentDate;
        this.hasAttachment = hasAttachment;
        this.newMessage = newMessage;
        this.seen = seen;
        this.uid = uid;
    }
}
