package com.sendmail.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Client implements UserDetails {

    private int id;

    private String email;

    private String password;

    private String imgUrl;

    private String name;

    private String female;

    private String city;

    private int[] mesCount;

    private String nickname;

    private String lastInetAddress;

    private String appPassword;

    private int cacheId;

    public Client(String email, String password,String appPassword, String imgUrl, String name, String female) {
        this.email = email;
        this.password = password;
        this.imgUrl = imgUrl;
        this.name = name;
        this.female = female;
        this.appPassword = appPassword;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
