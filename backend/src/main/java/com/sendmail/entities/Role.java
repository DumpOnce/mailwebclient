package com.sendmail.entities;

public enum Role {
    ADMIN,
    USER,
    BLOCKED
}
