package com.sendmail.entities;

import java.util.HashMap;

public class ServerMailPort { // We need app password everywhere except of gmail

    public static class MAIL{ // We need app password

        private static final short SMTP_PORT = 465;

        private static final short IMAP_PORT = 993 ;

        private static final String IMAP_SERVER = "imap.mail.ru";

        private static final String SMTP_SERVER = "smtp.mail.ru";

        private static final String INBOX_FOLDER = "INBOX";

        private static final String SENT_FOLDER = "Отправленные";

        private static final String SPAM_FOLDER = "Спам";

        private static final String NOTE_FOLDER = "Черновики";

        private static final String BASKET_FOLDER = "Корзина";
    }

    public static class GMAIL{
        private static final short SMTP_PORT = 587;

        private static final short IMAP_PORT = 993 ;

        private static final String IMAP_SERVER = "imap.gmail.com";

        private static final String SMTP_SERVER = "smtp.gmail.com";

        private static final String INBOX_FOLDER = "INBOX";

        private static final String SENT_FOLDER = "Sent";

        private static final String SPAM_FOLDER = "Spam";

        private static final String NOTE_FOLDER = "Notes";
    }

    public static class YAHOO{
        private static final short SMTP_PORT = 587;

        private static final short IMAP_PORT = 993 ;

        private static final String IMAP_SERVER = "imap.mail.yahoo.com";

        private static final String SMTP_SERVER = "smtp.mail.yahoo.com";

        private static final String INBOX_FOLDER = "INBOX";

        private static final String SENT_FOLDER = "Sent";

        private static final String SPAM_FOLDER = "Spam";

        private static final String NOTE_FOLDER = "Notes";
    }

    public static class YANDEX{ // SSL
        private static final short SMTP_PORT = 465;

        private static final short IMAP_PORT = 993 ;

        private static final String IMAP_SERVER = "imap.mail.ru";

        private static final String SMTP_SERVER = "smtp.mail.ru";

        private static final String INBOX_FOLDER = "INBOX";

        private static final String SENT_FOLDER = "Sent";

        private static final String SPAM_FOLDER = "Spam";

        private static final String NOTE_FOLDER = "Notes";
    }

    public static void getRealFolder(HashMap<String, String> props ,Class<?> server,String folder) throws NoSuchFieldException, IllegalAccessException {
        props.put("folder", server.getDeclaredField(folder.toUpperCase() + "_FOLDER").get(null).toString());
    }

    public static Class<?> getServer(String email) throws ClassNotFoundException {
        String[] host = email.split("@")[1].split("\\."); //noname@mail.ru <--mail
        HashMap<String, String> props = new HashMap<>();
        String str = host[0].toUpperCase();
        String path = "com.sendmail.entities.ServerMailPort$"+host[0].toUpperCase();
        return Class.forName(path);
    }

    public static HashMap<String, String> getHostByEmail(String email,String folder) throws  ClassNotFoundException, NoSuchFieldException, IllegalAccessException {

        String[] host = email.split("@")[1].split("\\."); //noname@mail.ru <--mail
        HashMap<String, String> props = new HashMap<>();
        String str = host[0].toUpperCase();
        String path = "com.sendmail.entities.ServerMailPort$"+host[0].toUpperCase();
        Class<?> server = getServer(email);

        props.put("imap_server",server.getDeclaredField("IMAP_SERVER").get(null).toString());
        props.put("smtp_server",server.getDeclaredField("SMTP_SERVER").get(null).toString());

        short smtp_port = server.getDeclaredField("SMTP_PORT").getShort(null);
        short imap_port = server.getDeclaredField("IMAP_PORT").getShort(null);

        props.put("smtp_port", String.valueOf(smtp_port));
        props.put("imap_port", String.valueOf(imap_port));

        if(folder!= null) {
            ServerMailPort.getRealFolder(props,server,folder);
        }
        return props;
    }

}
