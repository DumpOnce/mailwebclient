package com.sendmail.entities;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class Post {
    private String from;
    private String to;
    private String cc;
    private String bcc;

    private String subject;

    private String bodyText;

    private MultipartFile[] files;
}
