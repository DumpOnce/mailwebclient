package com.sendmail.utills;

import com.sendmail.entities.Client;
import com.sendmail.service.ClientServiceImpl;
import jakarta.mail.MessagingException;
import jakarta.mail.Store;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CommonUtils {
    public final static String REFRESH_TOKEN = "refresh_token";

    public final static String ACCESS_TOKEN = "access_token";

    public static void connect(Store store, Map<String,String> info, HashMap<String,String> main) throws MessagingException {
        if(main.containsKey("token")){
            store.connect(main.get("imap_server"), info.get("email"),main.get("token"));
        }
        else{
            store.connect(main.get("imap_server"), info.get("email"), info.get("password"));
        }
    }

    public static List<Client> getClientsFromPrincipals(List<Object> principals, ClientServiceImpl clientService){
        List<Client> list = new ArrayList<>();

        for(final Object principal : principals){
            list.add((Client)clientService.loadUserByUsername(principal.toString()));
        }
        return list;
    }
    public static void killExpiredSessionForSure(String id) {
        try {
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.add("Cookie", "JSESSIONID=" + id);
            HttpEntity requestEntity = new HttpEntity(null, requestHeaders);
            RestTemplate rt = new RestTemplate();
            rt.exchange("http://localhost:8098", HttpMethod.GET,
                    requestEntity, String.class);
        } catch (Exception ex) {}
    }
}
