package com.sendmail.utills;

public class Query { //  Нужны Join'ы

    public final static String DELETE_CLIENT_QUERY =
            "DELETE FROM client_db WHERE email =  ?;";

    public final static String REGISTER_CLIENT_QUERY =
            "INSERT INTO client_db (email,password,imgUrl,name,female)VALUES (?,?,?,?,?);";

    public final static String UPDATE_CLIENT_DATA =
            "UPDATE client_db SET imgUrl = ?, name=?,female=? WHERE id = ?;";

    public final static String GET_CLIENT_ID_BY_EMAIL =
            "SELECT * FROM client_db WHERE email = ?;";


    public final static String DELETE_BLACKLIST_PERSON_FROM_CLIENTS_LIST =
            "DELETE FROM bw_list_db WHERE bw_list_db.client_id = ? AND bw_list_db.list_member=?;";

    public final static String GET_CLIENT_BLACKLIST =
            "SELECT * FROM bw_list_db WHERE clientId = ?;";
    public final static String ADD_CLIENT_TO_BLACKLIST =
            "INSERT INTO bw_list_db (client_id,list_member) VALUES (?,?);";
    public final static String GET_CLIENT_BY_EMAIL =
            "SELECT * FROM client_db WHERE email = ?;";

    public final static String DELETE_USER_FROM_CLIENT_DB =
            "DELETE FROM client_dn WHERE email = ?;";
    public final static String DELETE_USER_FROM_BLACKLIST_DB =
            "DELETE FROM bw_list_db WHERE client_id = ?;";
    public final static String UPDATE_USER_IMG =
         "UPDATE client_db SET imgUrl = ? WHERE email = ?;";
    public final static String ADD_TO_MESSAGE_CACHE =
            "insert INTO messages_db (emailFrom,clientId, folderName, hasAttachment, receivedDate, sentData, isNew, emailTo, isSeen, uid, subject)VALUES (?,?,?,?,?,?,?,?,?,?,?);";

    public final static String GET_ALL_MESSAGES_FROM_CACHE =
            "            select * from messages_db where clientid = ? and foldername = ? order by(sentData)DESC limit ?  OFFSET ?;";

    public final static String GET_COUNT_OF_MESSAGES =
            "SELECT count(*) AS count FROM messages_db WHERE clientId = ? AND foldername = ?; ";
    public final static String DELETE_ALL_MESSAGES_FROM_FOLDER =
            "delete from messages_db where clientId = ? and foldername = ? ;";

    public final static String DELETE_ALL_MESSAGES =
                "delete from messages_db where clientId = ?;";

    public final static String CREATE_FOLDERS =
            "insert into folders_db (clientId, foldername) VALUES (?,?);";
    public final static String SET_SEEN =
            "update messages_db set isSeen = ? where clientId = ? and foldername = ? and uid = ? ;";
    public static class Sort{
        public final static String DATE_SORT =
               " select * from messages_db where clientId = ? and foldername = ? order by (sentdate) %s offset ?  limit ?;";
        public final static String HAS_ATTACHMENT =
                "select * from messages_db where clientId = ? and foldername = ? where hasattachment = %s order by (sentdate) %s offset ?  limit ? ;" ;
        public final static String READ_STATUS =
                "select * from messages_db where clientId = ? and foldername = ? where isseen = %s order by (sentdate) %s offset ?  limit ?" ;
    }
}
