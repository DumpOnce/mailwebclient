package com.sendmail.utills;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class Property {
    @Value("${jwt.secret}")
    private String TOKEN;
    public String getToken(){return TOKEN;}
}
