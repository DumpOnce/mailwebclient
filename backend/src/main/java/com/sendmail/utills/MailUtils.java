package com.sendmail.utills;

import com.sendmail.entities.Client;
import com.sendmail.entities.Envelope;
import com.sendmail.entities.ServerMailPort;
import com.sendmail.mail.MailAdminImpl;
import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class MailUtils {
    public final static String[] folders = {
            "INBOX",
            "SENT",
            "NOTE",
            "BASKET",
            "SPAM"
    };
    public final static String IMAP = "imaps";
    public final static String SMTP = "smtp";

    public static void fillMessages(Client client,MailAdminImpl mailAdmin) throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException, MessagingException {
        HashMap<String, String> map = new HashMap<>();
        String email  =  client.getEmail();
        String appPassword = client.getAppPassword();
        map.put("email", email);
        map.put("password",appPassword);
        String[] folders = { /// Раздробить функцию
                ServerMailPort.getHostByEmail(email, "INBOX").get("folder"),
                ServerMailPort.getHostByEmail(email, "SENT").get("folder"),
                ServerMailPort.getHostByEmail(email, "NOTE").get("folder"),
                ServerMailPort.getHostByEmail(email, "BASKET").get("folder"),
                ServerMailPort.getHostByEmail(email, "SPAM").get("folder"),
        };
        int i = 0;
        Store store = MailUtils.getStore(map);
        for (String folder : folders) {
            mailAdmin.getMessages(store,email,folder,MailUtils.folders[i++]);
        }
        store.close();
    }

    public static void acceptMessageRecipients(String recType, Message message, Message.RecipientType type) throws MessagingException {
        if(recType.equals("")){
            return;
        }
        String[] recipientList = recType.split(",");
        InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
        int counter = 0;
        for (String recipient : recipientList) {
            recipientAddress[counter++] = new InternetAddress(recipient.trim());
        }
        message.setRecipients(type, recipientAddress);
    }

    public static Session getSessionForSmtp(String email, String port, String host) throws NoSuchFieldException, ClassNotFoundException, IllegalAccessException, NoSuchProviderException {
        Properties props = new Properties();
        HashMap<String, String> main = ServerMailPort.getHostByEmail(email,null);

        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.host",host);
        props.put("mail.smtp.auth","true");
        props.put("mail.smtp.timeout", 1000);

        return Session.getInstance(props,null);
    }

    public static Properties getImapProperties(String port,String host){
        Properties props = new Properties();
        props.put("mail.imap.ssl.enable", "true");
        props.put("mail.imap.port", port);
        props.put("mail.imap.host", host);
        props.setProperty("mail.store.protocol", "imap");
        props.put("mail.imap.fetchsize", "819200");
        return props;
    }

    public static String getCleanString(Message message) throws MessagingException { /// todo with stream api
        return  message
                .getFrom()[0]
                .toString()
                .split(" ")[message
                .getFrom()[0]
                .toString()
                .split(" ").length-1].replaceAll("[<>]","");
    }

    public static String getText(Part p,boolean textIsHtml) throws
            MessagingException, IOException {
        if (p.isMimeType("text/*")) {
            String s = (String)p.getContent();
            textIsHtml = p.isMimeType("text/html");
            return s;
        }

        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart)p.getContent();
            String text = null;
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    if (text == null)
                        text = getText(bp,textIsHtml);
                    continue;
                } else if (bp.isMimeType("text/html")) {
                    String s = getText(bp,textIsHtml);
                    if (s != null)
                        return s;
                } else {
                    return getText(bp,textIsHtml);
                }
            }
            return text;
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart)p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getText(mp.getBodyPart(i),textIsHtml);
                if (s != null)
                    return s;
            }
        }
        return null;
    }

    public static Store getStore(HashMap<String, String> info) throws MessagingException, NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        HashMap<String, String> main = ServerMailPort.getHostByEmail(info.get("email"),null);
        String port = main.get("imap_port");
        String host = main.get("imap_server");
        Properties props = getImapProperties(port,host);

        Session session = Session.getInstance(props);
        session.setDebug(true);
        Store store = session.getStore("imaps");
        store.connect(host, info.get("email"), info.get("password"));
        return store;
    }
    public static void login( HashMap<String,String> body){

    }

    public static void getAttachmentFromMultipart(Multipart multipart, Envelope envelope) throws IOException {
        try {
            for (int i = 0; i < multipart.getCount(); i++) {

                BodyPart bp = multipart.getBodyPart(i);
                if (!bp.getContentType().contains("alternative")) {
                    InputStream stream = bp.getInputStream();

                    byte[] buffer = new byte[512];
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();

                    int bytesRead;
                    while ((bytesRead = stream.read(buffer)) != -1) {
                        baos.write(buffer, 0, bytesRead);
                    }
                    envelope.byteList.add(baos.toByteArray());
                }
            }
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}
