package com.sendmail.service;

import com.sendmail.entities.Client;
import com.sendmail.entities.Envelope;
import com.sendmail.exceptions.BadRealMailCredentialsException;
import com.sendmail.exceptions.ClientNotFoundException;
import com.sendmail.exceptions.PasswordNotMatchableException;
import com.sendmail.exceptions.WrongClientDataException;
import com.sendmail.repository.BlackListRepoImpl;
import com.sendmail.repository.ClientRepoImpl;
import com.sendmail.utills.MailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClientServiceImpl implements ClientService{

    @Autowired
    private ClientRepoImpl repo;

    @Autowired
    private BlackListRepoImpl  blackListRepo;

    @Override
    public void setSeenStatus(String email, String folder, int uid, boolean status) {
        repo.setSeenStatus(email,folder,uid,status);
    }

    @Override
    public void fillCache(String folder, List<Envelope> messages, String email) {
        repo.fillMyMessagesCache(folder,messages,email);
    }

    @Override
    public List<String> deletePersonFromBlacklist(String who, String whom) {
        blackListRepo.deleteClientFromBlacklist(who,whom);
        return blackListRepo.getAllBlacklistMembers(who);
    }

    @Override
    public List<String> getBlackList(String email) {
        return blackListRepo.getAllBlacklistMembers(email);
    }

    @Override
    public String getUserInfo(String email) {
        return null;
    }

    @Override
    public String getUserInfo(long id) throws ClientNotFoundException {
        return null;
    }

    @Override
    public void registerClient(HashMap<String,String> body) throws  WrongClientDataException {
        repo.addClient(body);
        repo.addFolders(body.get("email"));
    }

    @Override
    public void insertClientIntoBlackList(String who, String whom) {
        blackListRepo.addToBlackList(who,whom);
    }

    @Override
    public void updateUser(String principal, Client client) {
        repo.updateClient(principal,client);
    }

    @Override
    public void deleteUser(String email) {
        try {
            int id = blackListRepo.getIdByEmail(email);
            blackListRepo.deleteUser(id);
            repo.deleteClientByEmail(email);
        }
        catch(ClientNotFoundException e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void updateUserImg(String principal, byte[] bytes) {
        repo.updateImage(principal,bytes);
    }

    @Override
    public List<Envelope> getAllMessages(String email, String folder, int messageLimit,int offset) {
        return repo.getAllMessages(email,folder,messageLimit,offset);
    }

    @Override
    public void deleteAllMessagesFromFolder(String folder, String email) {
        repo.deleteAllMessages(folder,email);
    }

    @Override
    public void deleteAllMessages(String email) {
        repo.deleteAllMessages(email);
    }

    @Override
    public int[] getMessagesCount(String email) {
        int[] counts = new int[MailUtils.folders.length];
        int i = 0;
        for(String folder:MailUtils.folders){
           counts[i++] = repo.getCountOfFolderMessages(email,folder);
        }
        return  counts;
    }

    @Override
    public void showHasAttachment(String email, String folder, boolean b, int offset, int limit) {

    }

    @Override
    public void sortByDateAsc(String email, String folder, int offset, int limit) {
        repo.sortByDate(email,folder,offset,limit,false);
    }

    @Override
    public void showByReadStatus(String email, String folder, boolean b, int offset, int limit) {

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repo.getUser(username);
    }
}
