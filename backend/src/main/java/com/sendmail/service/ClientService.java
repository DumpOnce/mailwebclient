package com.sendmail.service;

import com.sendmail.entities.Client;
import com.sendmail.entities.Envelope;
import com.sendmail.exceptions.BadRealMailCredentialsException;
import com.sendmail.exceptions.ClientNotFoundException;
import com.sendmail.exceptions.PasswordNotMatchableException;
import com.sendmail.exceptions.WrongClientDataException;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.HashMap;
import java.util.List;

public interface ClientService extends UserDetailsService {
    void setSeenStatus(String email,String folder,int uid,boolean status);
    void fillCache(String folder, List<Envelope> messages,String email);

     List<String> deletePersonFromBlacklist(String who,String whom);

     List<String> getBlackList(String email);

     String getUserInfo(String email) throws ClientNotFoundException;

     String getUserInfo(long id) throws ClientNotFoundException;

     void registerClient(HashMap<String,String> info) throws BadRealMailCredentialsException, WrongClientDataException;

     void insertClientIntoBlackList(String who,String whom);

    void updateUser(String principal, Client client);

    void deleteUser(String email) throws ClientNotFoundException;

    void updateUserImg(String principal, byte[] bytes);

    List<Envelope> getAllMessages(String email,String folder,int messageLimit,int offset);

    void deleteAllMessagesFromFolder(String folder,String email);

    void deleteAllMessages(String email);

    int[] getMessagesCount(String email);

    void showHasAttachment(String email, String folder, boolean b, int offset, int limit);

    void sortByDateAsc(String email, String folder, int offset, int limit);

    void showByReadStatus(String email, String folder, boolean b, int offset, int limit);
}
