package com.sendmail.mail;

import com.sendmail.entities.Envelope;
import com.sendmail.entities.Post;
import com.sendmail.exceptions.UnresolvableHostException;
import jakarta.mail.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface MailAdmin {
    void login(Map<String,String> info);

    int[] getCountOfAllTopicMessages(HashMap<String, String> info);

    Envelope getMessageByUID(HashMap<String, String> info,long uid,String folder);

    List<Envelope> getMessages(Store store,String email, String folder,String folderOriginal);

    void sendMail(String email,String password, Post post) throws MessagingException, UnresolvableHostException, NoSuchFieldException, ClassNotFoundException, IllegalAccessException;
}