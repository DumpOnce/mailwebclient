package com.sendmail.mail;


import com.sendmail.exceptions.ClientAlreadyExistsException;
import com.sendmail.utills.MailUtils;
import com.sendmail.entities.Envelope;
import com.sendmail.entities.Post;
import com.sendmail.entities.ServerMailPort;
import com.sendmail.exceptions.UnresolvableHostException;
import com.sendmail.repository.BlackListRepoImpl;
import com.sendmail.service.ClientServiceImpl;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.smtp.SMTPTransport;
import jakarta.activation.DataHandler;
import jakarta.activation.DataSource;
import jakarta.mail.*;
import jakarta.mail.internet.*;
import jakarta.mail.util.ByteArrayDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.io.*;
import java.util.*;


@Service
public class MailAdminImpl implements MailAdmin{

    @Autowired
    private BlackListRepoImpl blackListRepo;

    @Autowired
    private ClientServiceImpl clientService;

    @Override
    public void /**/login(Map<String, String> info) /*throws ClientAlreadyExistsException */{
      //  throw new ClientAlreadyExistsException();
    }

    @Override
    public int[] getCountOfAllTopicMessages(HashMap<String, String> info) {
        try {
            Store store = MailUtils.getStore(info);
            String email = info.get("email");

            String[] folders = {
                    ServerMailPort.getHostByEmail(email,"INBOX").get("folder"),
                    ServerMailPort.getHostByEmail(email,"SENT").get("folder"),
                    ServerMailPort.getHostByEmail(email,"NOTE").get("folder"),
                    ServerMailPort.getHostByEmail(email,"BASKET").get("folder"),
                    ServerMailPort.getHostByEmail(email,"SPAM").get("folder"),
            };
            int[] count = new int[folders.length];
            for (int i = 0; i < folders.length; i++) {
                count[i] = store.getFolder(folders[i]).getMessageCount();
            }
            return count;
        } catch (Exception e) {
            return new int[0];
        }
    }

    @Override
    public Envelope getMessageByUID(HashMap<String, String> info,long uid,String folder) {
        try {
            String email = info.get("email");
            String password = info.get("password");
            HashMap<String, String> main = ServerMailPort.getHostByEmail(email,folder);
            String port = main.get("imap_port");
            String host = main.get("imap_server");

            Properties props = MailUtils.getImapProperties(port,host);

            Session session = Session.getInstance(props);
            session.setDebug(true);
            Store store = session.getStore(MailUtils.IMAP);
            store.connect(host, email, password);

            Folder fold = store.getFolder(main.get("folder"));

            fold.open(Folder.READ_WRITE);
            UIDFolder uf = (UIDFolder) fold;

            Message message =  uf.getMessageByUID(uid);
            message.setFlag(Flags.Flag.SEEN,true);
            clientService.setSeenStatus(email,folder.toUpperCase(),(int)uid,true);

            return getEnvelopeByMessage(uid,message);
        }
        catch(Exception e){
            return null;
        }
    }

    public Envelope getEnvelopeByMessage(long uid,Message message) throws MessagingException, IOException {
        Envelope env = new Envelope();
        env.setUid(uid);

        Flags flags = message.getFlags();

        if(message.getFlags().contains(Flags.Flag.SEEN)){
            env.setSeen(true);
        }

        if (message.isMimeType(MediaType.MULTIPART_MIXED_VALUE)) {
            Multipart mp = (Multipart) message.getContent();
            if (mp.getCount() > 1) {
                env.setHasAttachment(true);
              //  MailUtils.getAttachmentFromMultipart(mp,env);
            }
        }
        String subject = message.getSubject();

        Date sent = message.getSentDate();

        env.setFrom(decodeAddress(message.getFrom()[0].toString()));

        env.setSentDate(sent);

        env.setBodyText(MailUtils.getText(message,true));

        env.setSubject(subject);
        return env;
    }

    @Override
    public List<Envelope> getMessages(Store store,String email, String folder,String folderOriginal) {
        try {
            Folder f = store.getFolder(folder);

            f.open(Folder.READ_WRITE);
            UIDFolder uf = (UIDFolder)f;

            List<Envelope> envelopes = new ArrayList<>();
            Message[] mesgs = f.getMessages();
            FetchProfile fp = new FetchProfile();
            fp.add(FetchProfile.Item.ENVELOPE);
            fp.add(IMAPFolder.FetchProfileItem.FLAGS);
            fp.add(IMAPFolder.FetchProfileItem.CONTENT_INFO);

            fp.add("X-mailer");
            f.fetch(mesgs, fp);
            for(Message  message: mesgs) {
                String currentEmailFrom = MailUtils.getCleanString(message);
                if(!blackListRepo.checkIfClientInBlackList(email,currentEmailFrom)) {
                    Envelope env = getEnvelopeByMessage(uf.getUID(message), message);
                    envelopes.add(env);
                }
            }
            clientService.deleteAllMessagesFromFolder(folderOriginal,email);
            clientService.fillCache(folderOriginal,envelopes, email);
            return envelopes;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public String decodeAddress(String emailId) throws UnsupportedEncodingException {
        String string = "";
        try{
            string =  MimeUtility.decodeWord(emailId);
        }catch(ParseException e){
            System.out.println("ParseException occured so return the same string");
            string = emailId;

        }
        return string;
    }

    public void sendMailCheckToChangePort(String email,String password, Post post,String port,String host) throws MessagingException, NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        Session session = MailUtils.getSessionForSmtp(email,host,port);

        SMTPTransport transport = (SMTPTransport)session
                .getTransport(MailUtils.SMTP);
        transport.connect(host, email, password);

        Message message = new MimeMessage(session);

        MailUtils.acceptMessageRecipients(post.getTo(),message, Message.RecipientType.TO);
        MailUtils.acceptMessageRecipients(post.getCc(),message, Message.RecipientType.CC);
        MailUtils.acceptMessageRecipients(post.getBcc(),message, Message.RecipientType.BCC);

        message.setFrom(new InternetAddress(email));

        message.setSubject(post.getSubject());

        message.setSentDate(new Date());

        BodyPart messageBodyPart = new MimeBodyPart();

        messageBodyPart.setContent(post.getBodyText(), MediaType.TEXT_HTML_VALUE);

        Multipart multipart = new MimeMultipart();

        multipart.addBodyPart(messageBodyPart);

        MultipartFile[] files = post.getFiles();

        if(files != null && files.length > 0){
            for (MultipartFile filePath : files) {
                MimeBodyPart attachPart = new MimeBodyPart();
                try {
                    DataSource ds = new ByteArrayDataSource(filePath.getBytes(), filePath.getContentType());
                    attachPart.setDataHandler(new DataHandler(ds));
                    attachPart.setFileName(filePath.getOriginalFilename());
                    attachPart.setDisposition(Part.ATTACHMENT);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                multipart.addBodyPart(attachPart);
            }
        }

        message.setContent(multipart);

        transport.sendMessage(message,message.getAllRecipients());

        transport.close();
    }

    @Override
    public void sendMail(String email,String password, Post post) throws MessagingException, UnresolvableHostException, NoSuchFieldException, ClassNotFoundException, IllegalAccessException {
        HashMap<String, String> main = ServerMailPort.getHostByEmail(email,null);
        try {
            sendMailCheckToChangePort(email, password, post, main.get("smtp_port"), main.get("smtp_server"));
        } catch (Exception e) {
            sendMailCheckToChangePort(email, password, post, "25", main.get("smtp_server"));
        }
    }


}
