package com.sendmail.repository;

import com.sendmail.entities.Client;
import com.sendmail.utills.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Objects;

@Repository
public class BlackListRepoImpl implements BlackListRepo{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int getIdByEmail(String email){
        return  Objects.requireNonNull(jdbcTemplate.queryForObject(Query.GET_CLIENT_ID_BY_EMAIL, (rs, idx) -> {
            Client client = new Client();
            client.setId(rs.getInt("id"));
            return client;
        }, email)).getId();
    }

    @Override
    public void deleteUser(int id) {
        jdbcTemplate.update(Query.DELETE_USER_FROM_BLACKLIST_DB,id);
    }

    @Override
    public List<String> getAllBlacklistMembers(String email) {
        int id = getIdByEmail(email);
       return  jdbcTemplate.query(Query.GET_CLIENT_BLACKLIST,(rs,idx)->{
            return rs.getString("list_member");
        },id);
    }

 /*   @Override
    public List<String> getAllBlacklistMembers(int id) {
        return null;
    }
*/
    @Override
    public void addToBlackList(String who,String whom) {
        int id = getIdByEmail(who);
        jdbcTemplate.update(Query.ADD_CLIENT_TO_BLACKLIST,id,whom);
    }

    @Override
    public void deleteClientFromBlacklist(String who, String whom) {
        int id = getIdByEmail(who);
        jdbcTemplate.update(Query.DELETE_BLACKLIST_PERSON_FROM_CLIENTS_LIST,id,whom);
    }

    @Override
    public boolean checkIfClientInBlackList(String who, String whom) {
        return jdbcTemplate.query(Query.GET_CLIENT_BLACKLIST,(rs,idx)->{
            return rs.getString("list_member");
        },who).contains(whom);
    }
}
