package com.sendmail.repository;

import com.sendmail.entities.Client;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface BlackListRepo {
    void deleteUser(int id);
    List<String> getAllBlacklistMembers(String email);
    void addToBlackList(String who, String whom);
    void deleteClientFromBlacklist(String who,String whom);
    boolean checkIfClientInBlackList(String who, String whom);
}
