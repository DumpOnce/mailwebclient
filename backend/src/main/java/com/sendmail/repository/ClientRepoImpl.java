package com.sendmail.repository;

import com.sendmail.entities.Client;
import com.sendmail.entities.Envelope;
import com.sendmail.utills.Query;
import com.sendmail.exceptions.ClientNotFoundException;
import com.sendmail.exceptions.WrongClientDataException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class ClientRepoImpl implements ClientRepo {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public ClientRepoImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int getCountOfFolderMessages(String email, String folder) {
        return jdbcTemplate.queryForObject(Query.GET_COUNT_OF_MESSAGES,(rs,idx)->rs.getInt("count"),email,folder);
    }

    @Override
    public void deleteAllMessages(String email) {
        jdbcTemplate.update(Query.DELETE_ALL_MESSAGES,email);
    }

    @Override
    public List<Envelope> getHasAttachmentMessages(String email, String folder) {
        return null;
    }

    @Override
    public List<Envelope> getOldestMessagesFirst(String email, String folder) {
        return null;
    }

    @Override
    public void setSeenStatus(String email, String folder, int uid,boolean status) {
        jdbcTemplate.update(Query.SET_SEEN,email,folder,uid,status);
    }

    @Override
    public void fillMyMessagesCache(String folder, List<Envelope> messages,String email) {
        for(Envelope env:messages){
            jdbcTemplate.update(Query.ADD_TO_MESSAGE_CACHE,
                    env.getFrom(),
                    email,
                    folder,
                    env.isHasAttachment(),
                    env.getReceivedDate(),
                    env.getSentDate(),
                    env.isNewMessage(),
                    env.getTo(),
                    env.isSeen(),
                    env.getUid(),
                    env.getSubject());
        }
    }

    @Override
    public void updateImage(String email, byte[] image) {
        jdbcTemplate.update(Query.UPDATE_USER_IMG,image,email);
    }

    @Override
    public List<Client> getAllClients() {
        return null;
    }

    @Override
    public Client getUser(String email) {

        try {
            return jdbcTemplate.queryForObject(Query.GET_CLIENT_BY_EMAIL, (rs, idx) -> {
                Client client = new Client();
                client.setEmail(rs.getString("email"));
                client.setImgUrl(rs.getString("imgUrl"));
                client.setPassword(rs.getString("password"));
                client.setAppPassword(rs.getString("appPassword"));
                client.setName(rs.getString("name"));
                client.setFemale(rs.getString("female"));
                return client;
            }, email);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Client getUser(long id) {
        return null;
    }

    @Override
    public void deleteClientByEmail(String email) throws ClientNotFoundException {
        jdbcTemplate.update(Query.DELETE_USER_FROM_CLIENT_DB,email);
    }

    @Override
    public void addClient(HashMap<String,String> body) throws WrongClientDataException {
        jdbcTemplate.update(Query.REGISTER_CLIENT_QUERY,
                body.get("email"),
                body.get("password"),
                null,
                body.get("name"),
                body.get("female"));
    }

    @Override
    public void addToBlackListByEmail(String email) throws ClientNotFoundException {

    }

    @Override
    public void updateClient(String principal, Client client) {
        jdbcTemplate.update(Query.UPDATE_CLIENT_DATA,
                client.getImgUrl(),
                client.getName(),
                client.getFemale());
    }

    @Override
    public List<Envelope> getAllMessages(String email, String folder, int limit,int offset) {
       return (List<Envelope>)jdbcTemplate.query(Query.GET_ALL_MESSAGES_FROM_CACHE,(rs,idx)->{
            return new Envelope(
                    rs.getString("emailTo"),
                    rs.getString("emailFrom"),
                    rs.getString("subject"),
                    rs.getDate("receivedDate"),
                    rs.getDate("sentData"),
                    rs.getBoolean("hasAttachment"),
                    rs.getBoolean("isNew"),
                    rs.getBoolean("isSeen"),
                    rs.getInt("uid")
                    );
        },email,folder,limit,offset);
    }

    @Override
    public void deleteAllMessages(String folder, String email) {
        jdbcTemplate.update(Query.DELETE_ALL_MESSAGES_FROM_FOLDER,email,folder);
    }

    @Override
    public void addFolders(String email) {
        String[] folders = {
                "INBOX",
                "SENT",
                "SPAM",
                "BASKET",
                "NOTE"
        };
        for(String folder: folders){
            jdbcTemplate.update(Query.CREATE_FOLDERS,email,folder);
        }
    }

    @Override
    public List<Envelope> sortByDate(String email, String folder, int offset, int limit, boolean b) {
       return jdbcTemplate.query(Query.Sort.DATE_SORT.format((b?"DESC":"ASC")),(rs,idx)->{
           return new Envelope(
                   rs.getString("emailTo"),
                   rs.getString("emailFrom"),
                   rs.getString("subject"),
                   rs.getDate("receivedDate"),
                   rs.getDate("sentData"),
                   rs.getBoolean("hasAttachment"),
                   rs.getBoolean("isNew"),
                   rs.getBoolean("isSeen"),
                   rs.getInt("uid")
           );
       },email,folder,offset,limit );
    }
}
