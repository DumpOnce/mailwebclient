package com.sendmail.repository;

import com.sendmail.entities.Client;
import com.sendmail.entities.Envelope;
import com.sendmail.exceptions.ClientNotFoundException;
import com.sendmail.exceptions.WrongClientDataException;

import java.util.HashMap;
import java.util.List;

public interface ClientRepo {
    int getCountOfFolderMessages(String email, String folder);

    void deleteAllMessages(String email);

    List<Envelope> getHasAttachmentMessages(String email,String folder);

    List<Envelope> getOldestMessagesFirst(String email,String folder);

    void setSeenStatus(String email,String folder,int uid,boolean status);

     void fillMyMessagesCache(String folder,List<Envelope> messages,String email);

     void updateImage(String email,byte[] image);

     List<Client> getAllClients();

     Client getUser(String email);

     Client getUser(long id);

     void deleteClientByEmail(String email) throws ClientNotFoundException;

     void addClient(HashMap<String,String> body) throws WrongClientDataException;

     void addToBlackListByEmail(String email) throws ClientNotFoundException;

    void updateClient(String principal, Client client);

    List<Envelope> getAllMessages(String email,String folder,int limit,int offset);

    void deleteAllMessages(String folder, String email);

    void addFolders(String email);

    List<Envelope> sortByDate(String email, String folder, int offset, int limit, boolean b);
}