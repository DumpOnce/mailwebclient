package com.sendmail.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.sendmail.utills.CommonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;

import java.util.Date;
import java.util.HashMap;

@ComponentScan
public class JwtClass {

    @Value("${jwt.secret}")
    private String SECRET;

    public static HashMap<String,String> getJwtAccessRefreshTokens(String email, String password,String token){

        Algorithm algorithm = Algorithm.HMAC256(token);

        Date now = new Date();
        Date expire_access = new Date(now.getTime()+1000*1000*10);
        Date expire_refresh = new Date(now.getTime()+1000*1000*10*10);

        String access =
                JWT.create()
                        .withSubject(email)
                        .withClaim("pas",password)
                        .withExpiresAt(expire_access.toInstant())
                        .sign(algorithm);

        String refresh =
                JWT.create()
                        .withSubject(email)
                        .withClaim("pas",password)
                        .withExpiresAt(expire_refresh.toInstant())
                        .sign(algorithm);
        HashMap<String,String> map = new HashMap<>();
        map.put(CommonUtils.ACCESS_TOKEN,access);
        map.put(CommonUtils.REFRESH_TOKEN,refresh);
        return map;
    }

    public void JwtMatches(String jwt1,String jwt2){

    }
}
