package com.sendmail.config;

import com.sendmail.config.filters.MyAuthenticationFilter;
import com.sendmail.config.filters.MyOnePerRequestFilter;
import com.sendmail.entities.Role;
import com.sendmail.mail.MailAdminImpl;
import com.sendmail.service.ClientServiceImpl;
import com.sendmail.utills.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ClientServiceImpl clientService;

    @Autowired
    private Property property;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return  NoOpPasswordEncoder.getInstance();
    }

    @Autowired
    private MailAdminImpl mailAdmin;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(clientService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable();
        MyAuthenticationFilter myAuthenticationFilter =
                new MyAuthenticationFilter(authenticationManagerBean(),mailAdmin,property);
        myAuthenticationFilter.setFilterProcessesUrl("/api/login");

        http.headers().frameOptions().disable();
        http
                .sessionManagement()
                .maximumSessions(1).maxSessionsPreventsLogin(false).sessionRegistry(sessionRegistry()); // for jakarta mail connection
                http.authorizeRequests(
                req->req.antMatchers(HttpMethod.POST,"/registration/**")
                        .permitAll()
                        .antMatchers("/mail/**") // hasAuthority
                        .authenticated()
                        .antMatchers("/admin/**")
                        .hasRole(String.valueOf(Role.ADMIN)) // next release
        );
        http.addFilter(myAuthenticationFilter);
        http.addFilterBefore(new MyOnePerRequestFilter(),
                UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
}
