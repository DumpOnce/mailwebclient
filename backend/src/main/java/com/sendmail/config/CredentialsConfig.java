package com.sendmail.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.sendmail.mail.MailAdminImpl;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;
import java.util.Date;
import java.util.HashMap;

@Configuration
public class CredentialsConfig {



/*
    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource")
    public HikariDataSource getDataSource() {
        return DataSourceBuilder
                .create()
                .type(HikariDataSource.class)
                .build();
    }
*/

    @Bean
    @Primary
    @ConfigurationProperties("spring.datasource")
    public DataSource getDataSource() {
        return DataSourceBuilder
                .create()
                .build();
    }

    @Bean
    JdbcTemplate getJdbcTemplate(){
       // HikariDataSource hikariDataSource = getDataSource();
        DataSource dataSource = getDataSource();
        return new JdbcTemplate(/*hikariDataSource*/ dataSource);
    }

}
