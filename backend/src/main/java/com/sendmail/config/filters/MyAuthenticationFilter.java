package com.sendmail.config.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sendmail.config.JwtClass;
import com.sendmail.utills.Property;
import com.sendmail.entities.Client;
import com.sendmail.mail.MailAdminImpl;
import com.sendmail.utills.MailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

public class MyAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private Property property;

    @Autowired
    private AuthenticationManager manager;

    @Autowired
    private MailAdminImpl mailAdmin;

    public MyAuthenticationFilter(AuthenticationManager authenticationManager,MailAdminImpl mailAdmin,Property property) {
        this.manager = authenticationManager;
        this.mailAdmin = mailAdmin;
        this.property = property;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String email = request.getParameter("email");

        String password = request.getParameter("password"); // B64Crypted
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(email,password);
        return manager.authenticate(usernamePasswordAuthenticationToken);
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        Client client = (Client)authResult.getPrincipal();
        HashMap<String,String> tokens = JwtClass.getJwtAccessRefreshTokens(client.getEmail(),client.getAppPassword(),property.getToken());
        try {
            MailUtils.fillMessages(client, mailAdmin); /// fill cache
        }
        catch(Exception e){
            System.out.println(e.getMessage());
        }

        response.setContentType(APPLICATION_JSON_VALUE);
         SecurityContextHolder.getContext().setAuthentication(authResult);
        SecurityContext context = SecurityContextHolder.getContext();
        new ObjectMapper().writeValue(response.getOutputStream(),tokens);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, failed);
    }
}
