package com.sendmail.config.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sendmail.config.CredentialsConfig;
import com.sendmail.config.JwtClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MyOnePerRequestFilter extends OncePerRequestFilter {

    @Value("${jwt.secret}")
    private String SECRET;

    private static final Logger logger =
            LoggerFactory.getLogger(MyOnePerRequestFilter.class);

    public boolean isTokenExpired(Instant tokenExpires) {
        return ! Instant.now().isBefore(tokenExpires);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String path = request.getRequestURI();
        System.out.println(path);
        if(Objects.equals(path, "/api/login")||Objects.equals(path, "/refresh")||Objects.equals(path, "/registration")){
            filterChain.doFilter(request,response);
        }
        else {
            if (request.getHeader("Authorization") != null && request.getHeader("Authorization").startsWith("Bearer ")) {
                String token = request.getHeader("Authorization")
                        .substring("Bearer ".length());
                Algorithm algorithm = Algorithm.HMAC256("nkjberfuerjbrhfeirh35849fn");

                JWTVerifier verifier = JWT.require(algorithm).build();
                try {
                    DecodedJWT decodedJWT = verifier.verify(token);
                    Instant date = JWT.decode(token).getExpiresAtAsInstant();

                    if (isTokenExpired(date)) {
                        System.out.println("refresh redirect");
                        response.setStatus(HttpStatus.UNAUTHORIZED.value());
                        Map<String,String> err = new HashMap<>();
                        err.put("refresh","need to refresh token");
                        response.setStatus(HttpStatus.FORBIDDEN.value());
                        new ObjectMapper().writeValue(response.getOutputStream(),err);
                        return;
                    }

                    String email = decodedJWT.getSubject();
                    String pas = (decodedJWT.getClaim("pas")).asString();

                    UsernamePasswordAuthenticationToken usernamePasswordAuthenticationTokenu =
                            new UsernamePasswordAuthenticationToken(email, pas, null);
                    SecurityContextHolder
                            .getContext()
                            .setAuthentication(usernamePasswordAuthenticationTokenu);
                    filterChain.doFilter(request, response);
                } catch (Exception e) {
                    Map<String,String> err = new HashMap<>();
                    err.put("error",e.getMessage());
                    response.setStatus(HttpStatus.FORBIDDEN.value());
                    new ObjectMapper().writeValue(response.getOutputStream(),err);

                    logger.warn(e.getMessage());
                   // throw new RuntimeException("invalid jwt");
                }
            }
            else{
                filterChain.doFilter(request, response);
            }
        }
/*        response.setStatus(HttpStatus.FORBIDDEN.value());
        filterChain.doFilter(request,response);*/
    }
}
