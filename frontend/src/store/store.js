import {createStore} from "vuex";
import {DataGetType} from "@/types/DataGetType";
import {server} from '@/api/server'
import {data} from '@/api/interceptor'

export default createStore({
    state() {
        return {
            personalData: null,
            sendWindowIsOpen: false,
            isLoggedIn: false,
            sentHint: false,
            loadingStatus: DataGetType.LOADING,
            emails: null,
        }
    },
    getters:{
        sendWindowIsOpen:store=>store.sendWindowIsOpen(),
        getMessageData:(state)=>{
            state.messageData
        },
        getLoadingStatus:(state)=>{
            state.loadingStatus
        },
        getEmails:(state)=>state.emails,
        getAccountData:(state)=>state.account,
        getPersonalMessageData:(state)=>state.personalData,
    },
    actions:{
        fetchAccountData({commit}){
            data.get(server+"/mail/users/me",{})
                .then(response=>{
                    console.log(response);
                    commit('setAccountData',response.data);
                })
        },
        fetchPersonalMessageData({commit},path){
            commit('changeLoadingStatus',DataGetType.LOADING);
            console.log(path);
            data.get(server+'/mail/messages' +path, {})
                .then(resp => {
                    console.log(resp.data);
                    commit('setPersonalMessageData',resp.data);
                    commit('changeLoadingStatus', DataGetType.READY,path);// ???
                })
        },

        openCloseWindow(state){
            state.sendWindowIsOpen = !state.sendWindowIsOpen;
        },

        leavePage({commit}){
            commit('changeMessageData',null);
            commit('changeLoadingStatus',DataGetType.EMPTY);
        }
    },
    mutations:{
        setPersonalMessageData(state,data){
          state.personalData = data;
        },
        setAccountData(state,data){
            localStorage.setItem("id",data.id);
            localStorage.setItem("female",data.female);
            localStorage.setItem("name",data.name);
            localStorage.setItem("email",data.email);
            if(data.imgUrl!== '') localStorage.setItem("imgUrl",(data.imgUrl));
            else localStorage.setItem("imgUrl",'');
        },
        setEmails(state,emails){
            state.emails = emails;
        },

        changeLoadingStatus(state,status){
            state.loadingStatus = status;
        },
        changeMessageData(state,data){
            state.messageData = data;
        }
    }
})