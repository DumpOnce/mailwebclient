import axios from "axios";

import {server} from '@/api/server'

export const data = axios.create();


data.interceptors.request.use(config=>{
       console.log("in interceptor")
        console.log(config)
        config.headers = {
            'Authorization': `Bearer ${localStorage.getItem('access_token')}`,
            'Access-Control-Allow-Origin': '*'
        }
    console.log('giving response')
    return config;
},error=>{
    console.log('err')
    return Promise.reject(error);
});

data.interceptors.response.use(response=>response, error=> {console.log(error)
   if(error.response) {
       if (error.response.data.refresh !== null) {

           return axios.get(server + '/refresh', {
               headers: {
                   'Authorization': 'Bearer ' + localStorage.getItem('refresh_token'),
                   'Access-Control-Allow-Origin': '*'
               }
           }).then(response => {
               console.log(response);
               localStorage.setItem('access_token', response.data.access_token);
               localStorage.setItem('refresh_token', response.data.refresh_token);

               error.response.config.headers['Authorization'] = 'Bearer ' + response.data.access_token;

               return axios(error.response.config);
           }).catch(error => {
               console.log('not authorized');
               // this.router.push('/login');
               return Promise.reject(error);
           })
       } else {
           console.log('bad news');
           localStorage.clear();
           return Promise.reject(error);
           //  this.router.push('/login');
       }
   }
});
