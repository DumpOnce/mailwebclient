export const DataGetType = {
    EMPTY:'EMPTY',
    ERROR:'ERROR',
    LOADING:'LOADING',
    READY:'READY'
}