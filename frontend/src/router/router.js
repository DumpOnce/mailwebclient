import {createRouter, createWebHistory} from "vue-router";
const routes = [
    {
        path: '/',
        name:'home',
        redirect: '/inbox'
    },
    {
        path:'/inbox',
        name:'inbox',
        component:()=>import('@/pages/MyMainPage')
    },
    {
        path:'/notes',
        name:'notes',
        component:()=>import('@/pages/MyMainPage')
    },
    {
        name:'users',
        path:'/users/:id',
        component: ()=>import('@/pages/UserPage')
    },
    {
      name:'pagination',
      path:'/pagination',
      component:()=>import('@/components/auxcomponents/MyPagination')
    },
    {
        name:'me',
        path:'/users/me',
        component: ()=>import('@/pages/UserPage')
    },

    {
        name:'login',
        path:'/login',
        component:()=>import('@/pages/LoginPage')
    },
    {
        name:'logout', // todo: make redirect
        path:'/logout'
    },
    {
        name:'registration',
        path:'/register',
        component:()=>import('@/pages/RegistrationPage')
    },
    {
        path:"/admin",
        name:"admin",
        component:()=>import('@/pages/errors/ComingSoon')
    },
    {
        path: '/server/err',
        name: 'serverErr',
        component: () => import('@/pages/ServerErrorPage')
    },
    {
        name:'settings',
        path:'/settings',
        component:()=>import('@/pages/SettingsPage')
    },
    {
        name:'blacklist',
        path:'/blacklist',
        component:()=>import('@/pages/BlacklistPage')
    },
    {
        name:'basket',
        path:'/basket',
        component:()=>import('@/pages/MyMainPage')
    },
    {
        name:'spam',
        path:'/spam',
        component:()=>import('@/pages/MyMainPage')
    },
    {
        name:'sent',
        path:'/sent',
        component:()=>import('@/pages/MyMainPage')
    },
    {
        path:'/:folder/:msgId', // todo mesPage for every folder
        name:'folderMessages',
        component:()=>import ('@/pages/PostPage')
    },
    {
      path:'/chatroom',
      name:'chatroom',
      component:()=>import('@/pages/MailChatPage')
    },
    {
        path: "/:catchAll(.*)",
        name: "NotFound",
        component:()=>import('@/pages/errors/PageNotFound')
    }
]


const router = createRouter({
    history: createWebHistory(),
    routes
})
/*router.beforeEach((to,from,next)=>{ // todo logout control
    if((localStorage.getItem("access_token") === null && localStorage.getItem("refresh_token") === null) && to.path!=='/login' && to.path!=='/register')
    {
        next({path:'/login'});
        console.log("login");
    }
   else{
       next();
   }
})*/

export default router