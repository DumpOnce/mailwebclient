import {createApp} from "vue"
import App from './App'
import router from "@/router/router"
import store from '@/store/store'

import './assets/tailwind.css'



const app = createApp(App);
app.use(store);
app.use(router);
app.mount('#app');