/** @type {import('tailwindcss').Config} */
module.exports = {

  future: {},
  content: ["./src/**/*.{vue,js,ts,tsx,jsx}", "./public/**/*.html"],
  theme: {
    extend: {},
  },
  plugins: [
      require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/typography'),
    require('tailwind-scrollbar'),
      require('vue-tailwind-modal')
  ],
}
